/**
 * Idea: to use named arguments in defining javascript functions in order to provide more valuable information 
 */
function createADOMNode_named({ href, className, dataIsFormControl, title, dataValue, innerHTML }) {
    const aChild = document.createElement('a');
    aChild.href = href;
    aChild.className = className;
    aChild.setAttribute('data-isformcontrol', dataIsFormControl);
    aChild.setAttribute('title', title);
    aChild.setAttribute('dataValue', dataValue);
    aChild.innerHTML = innerHTML;

    return aChild;
}

// what the actual parameter definition looks like
const actualParams = {
    href: href,
    className: className,
    dataIsFormControl: dataIsFormControl,
    title: title,
    dataValue: dataValue,
    innerHTML: innerHTML
};

const namedInvocation = createADOMNode_named({
    href: `javascript:$('#AssignmentTable').data('treeActions').updateValue(event, '${currentNode.id}','${currentNode.id}');`,
    className: 'AspNet-TreeView-ClickableLink',
    dataIsFormControl: 'False',
    title: `${currentNode.id}`,
    dataValue: `${currentNode.id}`,
    innerHTML: `${currentNode.id}`
});


function createADOMNode_notNamed(href, className, dataIsFormControl, title, dataValue, innerHTML) {
    const aChild = document.createElement('a');
    aChild.href = href;
    aChild.className = className;
    aChild.setAttribute('data-isformcontrol', dataIsFormControl);
    aChild.setAttribute('title', title);
    aChild.setAttribute('dataValue', dataValue);
    aChild.innerHTML = innerHTML;

    return aChild;
}

const standardInvocation = createADOMNode_notNamed(
    `javascript:$('#AssignmentTable').data('treeActions').updateValue(event, '${currentNode.id}','${currentNode.id}');`,
    'AspNet-TreeView-ClickableLink',
    'False',
    `${currentNode.id}`,
    `${currentNode.id}`,
    `${currentNode.id}`
);

// can provide named arguments in any order desired
// named arguments are bound by name and not by positional context

function concatTwoStrings({ string1, string2 }) {
    return string1 + string2;
}

const concat_inOrder = concatTwoStrings({ string1: 'this is ', string2: 'a test' });
const concat_outOfOrder = concatTwoStrings({ string2: 'a test', string1: 'this is ' });

concat_inOrder === concat_outOfOrder; // true!!!

// Defualt params - variety of ways to handle
function concatTwoStrings_defaultParams({ string1 = 'no string1 provided ', string2 = ' no string2 provided' }) {
    return string1 + string2;
}

let test1 = concatTwoStrings_defaultParams({}); // works - both default
let test2 = concatTwoStrings_defaultParams(); // errors - Uncaught TypeError: Cannot destructure property `string1` of 'undefined' or 'null'.
let test3 = concatTwoStrings_defaultParams({ string1: 'test' }); // works as the unprovided string defaults

/**
 * Fixing test case 2 
 *  not normally an issue nor should it be suppressed as is being done here when invoked incorrectly
 *  this is, however, a fix to enable a function to be called without any params if desired
 * IF switching to named arguments, I think the better proposal would be that all functions ought to be invoked with an argument object EVEN IF ITS AN EMPTY ONE
 *  the absence of one should notify a user
 */
function concatTwoStrings_default_failSafe({ string1, string2 } =
    { string1: 'no string1 provided ', string2: ' no string2 privided' }) {

    return string1 + string2;
}

// more verbose, but truly enables you to pass in no params and it will still operate without error
let test4 = concatTwoStrings_default_failSafe();// success and params default

/**
 * Main takeaways
 *  Named arguments provide a more readable template for invoking functions
 *      this makes it easier to decipher code when approaching it initially as it is MORE descriptive of what is occurring 
 *  Named arguments vs typed positional arguments
 *      typed positional arguments are the traditional form of arguments in languages like Java, C#, Objective C and Swift
 *      typed positional arguments bind a parameter to a position and a specific type, named binds to name and is position free
 *      a few typed languages support annotations on invocation like swift: example("string": String, "another string": String)
 *          these typed annotations are great for whenever you are invoking a function as they are more descriptive than the lack of annotations BUT in many cases they still bind to a position
 *          additionally they still add some overhead when jumping from the function invokation to the function implementation as you are only binding to types and position instead of names, which is what the function uses when invoked
 *      ultimately this comparison has little merit though as this MAY be possible in typescript and this is more about improving the coding style in javascript not comparing to other languages
 *  
 *  what situations are named arguments less suited for?
 *  what situations are they more suited for?
 */